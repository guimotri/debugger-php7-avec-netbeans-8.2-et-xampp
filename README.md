# Debugger PHP7 avec Netbeans 8.2 et Xampp #


## 1. INSTALLER L'EXTENSION XDEBUG (.dll)##

* Récupérer la configuration PHP `phpinfo()` du serveur XAMPP (http://localhost/dashboard/phpinfo.php)

* Copier le code source de la page

* Se rendre sur la page https://xdebug.org/wizard.php

* Coller le code source et cliquer sur "Analyse my phpinfo() output"

* Après analyse, télécharger le fichier dll proposé et l'enregistrer dans "D:\xampp\php\ext"

## 2. XAMPP - PARAMÉTRER PHP ##

* Editer le fichier "D:\xampp\php\php.ini" et ajouter/modifier la section XDebug comme ci-dessous:

  _zend_extension doit pointer vers la .dll télécharger_
    
```ini
zend_extension = "d:\xampp\php\ext\php_xdebug-2.5.0-7.0-vc14.dll"
xdebug.idekey = "netbeans-xdebug"
xdebug.remote_autostart = 1
xdebug.profiler_append = 0
xdebug.profiler_enable_trigger = 0
xdebug.profiler_output_dir = "d:\xampp\tmp"
xdebug.remote_enable = 1
xdebug.remote_handler = "dbgp"
xdebug.remote_host = "127.0.0.1"
xdebug.remote_log = "d:\xampp\tmp\xdebug.txt"
xdebug.remote_port=9000
xdebug.trace_output_dir = "d:\xampp\tmp"
xdebug.remote_cookie_expire_time = 36000
```

* Toujours dans le php.ini, désactiver output buffering :

```ini
output_buffering = Off
```

* Redémarrer Apache

## 3. NETBEANS 8.2 - PARAMÉTRER PHP ##

* Options générales de PHP (menu "Outils"->"Options"->"PHP"->"General")

    ![01_Netbeans_opt_gen_php.JPG](img/01_Netbeans_opt_gen_php.JPG)

    __PHP 5 Interpreter__ : Pointer vers l'executable PHP du serveur XAMPP


* Options de debug de PHP (menu "Outils"->"Options"->"PHP"->"Debugging")

    ![02_Netbeans_opt_debug_php.JPG](img/02_Netbeans_opt_debug_php.JPG)

    __Debugger port__ : le même que celui spécifié dans le php.ini (9000 par défaut)

    __Session ID__ : le même que celui spécifié dans le php.ini

## 4. NETBEANS 8.2 - PARAMÉTRER LE PROJET PHP POUR LE DEBUG ##

  * Click droit sur projet -> "propriétés" -> "Run Configuration"

    ![03_Netbeans_projet_run_config.JPG](img/03_Netbeans_projet_run_config.JPG)

    __Run As__ : Local Web Site

    __Project URL__ : url du projet php

    __Index File__ : 

    __Arguments__ : 

## 5. NETBEANS 8.2 - DEBUGGER LE PROJET PHP 7 ##

  * Sélectionner le projet

  * Lancer le debug : `CTRL` + `F5`

  * Debug pas à pas : `F7`